#!/usr/bin/env python3
import random

#TODO: non capitalised seeds
#TODO new seeds in general
def parser(textfile):
    return textfile.replace('\n',' ').split()[0:-1]

class Grapher:

    def graph(self,words,n):
        graff = {}
        for i in range(len(words) - n):
            gram = tuple(words[i:i + n])
            if gram in graff:
                graff[gram].append(words[i + n])
            else:
                graff[gram] = [words[i + n]]
        return graff

    def dictPrint(self,d):
        for item in d:
            print(item, d[item])

def generate(initial, graph):
    out = initial
    if tuple(initial) in graph:
        rand = random.randint(0,len(graph[tuple(initial)]) - 1)
        out.append(graph[tuple(initial)][rand])
    else:
        pass
    return out

def sentence(d):
    while True:
        seed = list(random.choice(list(d)))
        if seed[0][0] in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
            break
        else:
            pass
    
    o = seed[0:2]
    for i in range(300):
        lol = generate(seed,d)
        seed = lol[-2:]
        o.append(seed[-1])
        if seed[-1][-1] == '.':
            break
    s = ''
    for i in o:
        s += i
        s += ' '
    return s

def speak():
    with open("corpus.txt","r") as f:
        text = f.read()
        old = [ i.lstrip() for i in text.split('\n') ]
        g = Grapher()
        d = g.graph(parser(text),2)

        while True:
            p = ''
            for i in range(1):
                y = sentence(d)
                p += y

            if p[:-1] in old:
                pass
            else:
                s = (p[:-2])
                break
        return s

if __name__ == "__main__":
    print(speak())
