#!/usr/bin/env python3
import socket
import markov
import time
import config

s = socket.socket()
s.connect((config.server, config.port))
s.send(bytes("NICK %s\n" % config.nick, "UTF-8"))
s.send(bytes("USER %s %s unused :%s\n\n" % (config.mode, config.server, config.realname), "UTF-8"))
s.send(bytes("JOIN %s\n" % config.room, "UTF-8"))

while True:
    srvmsg = '' + s.recv(1024).decode("UTF-8")

    for i in srvmsg.split("\n")[:-1]:
        i = i.split()
        #print(i)
        print(srvmsg)

        if i[0] == "PING":
            s.send(bytes("PONG %s\n" % i[1], "UTF-8"))
            s.send(bytes("JOIN %s\n" % config.room, "UTF-8"));
        if "PRIVMSG" in i[:]:
            for nick in config.nicks:
                for word in i[3:]:
                    if nick in word.lstrip(':'):
                        message = (":%s" % markov.speak())
                        message.lstrip(":")
                        time.sleep(4)
                        s.send(bytes("PRIVMSG %s %s \n" % ("%s" % config.room, message), "UTF-8"))
